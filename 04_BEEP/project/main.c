#include "stdio.h"
#include "cc.h"
#include "bsp_clk.h"
#include "bsp_delay.h"
#include "bsp_led.h"
#include "bsp_uart.h"
#include "bsp_beep.h"
int main(int argc, const char * argv[])
{
	clk_enable(); /* 使能所有的时钟 */
	led_init();	  /* 初始化led      */
	beep_init();  /* 初始化beep     */
	uart_init(); /* 初始化串口，波特率115200 */

	while (1)
	{
		/* 打开LED0和蜂鸣器 */
		led_switch(LED0,ON);	
		beep_switch(ON);
		delay(500);

		/* 关闭LED0和蜂鸣器 */
		led_switch(LED0,OFF);	
		beep_switch(OFF);
		delay(500);
	}

	return 0;
}

