#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : 1.sh
# * Author     : ssq
# * Date       : 2023-07-25
# * ======================================================
##

SCRIPT_NAME=${0#*/}    # 脚本名
SCRIPT_PARAM_ARGC=$#   # 传入的参数个数
SCRIPT_RELATIVE_PATH=${0%/*}
SCRIPT_ABSOLUTE_PATH_TEMP=$(readlink -f "$0")
SCRIPT_ABSOLUTE_PATH=${SCRIPT_ABSOLUTE_PATH_TEMP%/*} # 脚本所在的绝对路径
SCRIPT_IN_DIR_NAME=${SCRIPT_ABSOLUTE_PATH##*/}       # 脚本所在的上一级目录名称

##======================================================
BLACK="\033[1;30m"
RED='\033[1;31m'    # 红
GREEN='\033[1;32m'  # 绿
YELLOW='\033[1;33m' # 黄
BLUE='\033[1;34m'   # 蓝
PINK='\033[1;35m'   # 紫
CYAN='\033[1;36m'   # 青
WHITE='\033[1;37m'  # 白
CLS='\033[0m'       # 清除颜色

INFO="${GREEN}INFO  ${CLS}"
WARN="${YELLOW}WARN  ${CLS}"
ERR="${RED}ERROR  ${CLS}"
MAKE_PARAM=() # 定义一个空数组用于后边make参数
# 下载工具路径配置
IMXDOWNLOAD_TOOL=${SCRIPT_ABSOLUTE_PATH}/tools/imxdownload/imxdownload
MKIMAGE_TOOL=${SCRIPT_ABSOLUTE_PATH}/tools/mkimage/mkimage
MKIMAGE_CFG_FILE=${SCRIPT_ABSOLUTE_PATH}/tools/mkimage/imximage.cfg.cfgtmp

# Makefile子文件路径
MAKEFILE_COMPILE_TOOL_CFG=${SCRIPT_ABSOLUTE_PATH}/buildcfg/toolcfg.mk
MAKEFILE_INCLUDE_CFG=${SCRIPT_ABSOLUTE_PATH}/buildcfg/includecfg.mk
MAKEFILE_SOURCE_CFG=${SCRIPT_ABSOLUTE_PATH}/buildcfg/sourcecfg.mk
MAKE_PARAM[${#MAKE_PARAM[*]}]="COMPILE_TOOL_CFG=${MAKEFILE_COMPILE_TOOL_CFG}"
MAKE_PARAM[${#MAKE_PARAM[*]}]="INCLUDE_CFG=${MAKEFILE_INCLUDE_CFG}"
MAKE_PARAM[${#MAKE_PARAM[*]}]="SOURCE_CFG=${MAKEFILE_SOURCE_CFG}"

# 生成的目标文件名及烧写位置配置
TARGET=demo
TARGET_FILE=${TARGET}.bin
SD_NODE=/dev/sdc
BIN_START_ADDRESS=0X87800000  # 注意这里要和 imx6ul.lds 中保持一致
MAKE_PARAM[${#MAKE_PARAM[*]}]="IMX_NAME=${TARGET}"

##======================================================

function clean_project()
{
    make ${MAKE_PARAM[@]} clean
}

function build_project()
{
    if [ -f "${TARGET_FILE}" ];then
        echo -e "${INFO}清理工程文件..."
        clean_project
    fi

    echo -e "${INFO}编译工程..."
    make ${MAKE_PARAM[@]}

    echo -e "${INFO}检查是否编译成功..."
    if [ ! -f "${TARGET_FILE}" ];then
        echo -e "${RED}++++++++++++++++++++++++++++++++++++++++++++++++++++${CLS}"
        echo -e "${ERR}${TARGET_FILE} 编译失败,请检查后重试"
        echo -e "${RED}++++++++++++++++++++++++++++++++++++++++++++++++++++${CLS}"
    else
        echo -e "${GREEN}++++++++++++++++++++++++++++++++++++++++++++++++++++${CLS}"
        echo -e "${INFO}${TARGET_FILE} 编译成功"
        echo -e "${GREEN}++++++++++++++++++++++++++++++++++++++++++++++++++++${CLS}"
    fi
}
# 使用正点原子提供的工具生成并下载imx文件，但是有一个问题就是bin文件起始地址写死了，无法修改，除非修改工具源码
# 重新编译工具才行
function imxdownload_create_and_download_imx()
{
    # 判断编译目标文件是否存在
    if [ ! -e "${SD_NODE}" ];then
        echo -e "${ERR}${SD_NODE}不存在,请检查SD卡是否插入..."
        return
    fi
    # 检查bin文件是否存在
    if [ ! -f "${TARGET_FILE}" ];then
        echo -e "${ERR}${TARGET_FILE} 不存在,请检查后再下载..."
        return
    fi

    ${IMXDOWNLOAD_TOOL} ${TARGET_FILE} ${SD_NODE} # 会生成IMX文件并下载到SD卡
}

# 使用uboot中提供的工具生成和下载imx文件，好处是可以实现重定义，比较方便
function mkimage_create_and_download_imx()
{
    # 检查bin文件是否存在
    if [ ! -f "${TARGET_FILE}" ];then
        echo -e "${ERR}${TARGET_FILE} 不存在,请检查后再下载..."
        return
    fi
    sudo ${MKIMAGE_TOOL} -n ${MKIMAGE_CFG_FILE} -T imximage -e ${BIN_START_ADDRESS} -d ${TARGET_FILE} ${TARGET}_mkimage.imx
    # 判断编译目标文件是否存在
    if [ ! -e "${SD_NODE}" ];then
        echo -e "${ERR}${SD_NODE}不存在,请检查SD卡是否插入..."
        return
    fi
    sudo dd if=${TARGET}_mkimage.imx of=${SD_NODE} bs=1k seek=1 conv=fsync
}

function build_download_project()
{
    build_project
    #imxdownload_create_and_download_imx
    mkimage_create_and_download_imx
}

# 打印传输给make的各个参数
function func_print_make_param()
{
    for temp in ${MAKE_PARAM[@]}
        do
            echo ${temp}
        done
}

function echo_menu()
{
    echo "================================================="
	echo -e "${GREEN}               build project ${CLS}"
	echo -e "${GREEN}           ${SCRIPT_NAME} created by @ssq    ${CLS}"
	echo "================================================="
    echo -e "${PINK}current path    :$(pwd)${CLS}"
    echo -e "${PINK}SCRIPT_PATH     :${SCRIPT_ABSOLUTE_PATH}${CLS}"
    echo -e "${PINK}IMXDOWNLOAD_TOOL:${IMXDOWNLOAD_TOOL}${CLS}"
    echo -e "${PINK}MKIMAGE_TOOL    :${MKIMAGE_TOOL}${CLS}"
    echo -e "${PINK}MKIMAGE_CFG_FILE:${MKIMAGE_CFG_FILE}${CLS}"
    echo ""
    echo -e "* [0] 编译工程"
    echo -e "* [1] 清理工程"
    echo -e "* [2] 下载目标文件到SD卡"
    echo -e "*${GREEN} [3] 编译并下载目标文件到SD卡${CLS}"
    echo "================================================="
    echo "make param:"
    func_print_make_param
    echo "================================================="
}

function func_process()
{
	read -p "请选择功能,默认选择3:" choose
	case "${choose}" in
		"0") build_project;;
		"1") clean_project;;
		"2") imxdownload_create_and_download_imx;;
		"3") build_download_project;;
		*) build_download_project;;
	esac
}

echo_menu
func_process