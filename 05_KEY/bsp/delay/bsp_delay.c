#include "bsp_delay.h"

/**
  * @brief  短时间延时函数
  * @note   
  * @param  n 要延时循环次数(空操作循环次数，模式延时)
  * @retval 
  */
void delay_short(volatile unsigned int n)
{
	while(n--){}
}

/**
  * @brief  延时函数,在396Mhz的主频下,延时时间大约为1ms
  * @note   
  * @param  n 要延时的ms数
  * @retval 
  */
void delay(volatile unsigned int n)
{
	while(n--)
	{
		delay_short(0x7ff);
	}
}

