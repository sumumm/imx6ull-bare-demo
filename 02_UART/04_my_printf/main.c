#include "uart.h"
#include "my_printf.h"

char g_charA = 'A';       // 存储在 .data 段
const char g_charB = 'B'; // 存储在 .rodata 段
const char g_charC; // 存储在 .bss 段
int g_intA = 0;     // 存储在 .bss 段
int g_intB;         // 存储在 .bss 段

int main(int argc, const char * argv[])
{
	int c = 9;
 	Uart_Init();
	my_printf_test();	
	printf("\r\n");//反过来会报错，好像是因为库的冲突，加上-nostdinc -fno-builtin就好了
	printf("g_charA=%d &g_charA=0x%x\r\n", g_charA, &g_charA);
	printf("g_charB=%d &g_charB=0x%x\r\n", g_charB, &g_charB);
	printf("g_charC=%d &g_intA=0x%x\r\n", g_charC, &g_charC);
	printf("g_intA=%d &g_intA=0x%x\r\n", g_intA, &g_intA);
	printf("g_intB=%d &g_intB=0x%x\r\n", g_intB, &g_intB);
	printf("c=%d &c=0x%x\r\n", c, &c);
	return 0;
}


