#include "cc.h"
#include "bsp_clk.h"
#include "bsp_delay.h"
#include "bsp_led.h"
#include "bsp_uart.h"

int main(int argc, const char * argv[])
{
	unsigned char a = 0;
	unsigned char state = OFF;

	clk_enable(); /* 使能所有的时钟 			*/
	led_init();	  /* 初始化led 			*/

	uart_init(); /* 初始化串口，波特率115200 */

	while (1)
	{
		puts("请输入1个字符:");
		a = getc();
		putc(a); // 回显功能
		puts("\r\n");

		// 显示输入的字符
		puts("您输入的字符为:");
		putc(a);
		puts("\r\n\r\n");

		state = !state;
		led_switch(LED0, state);
	}

	return 0;
}
