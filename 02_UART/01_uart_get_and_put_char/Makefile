##============================================================================#
# Copyright ? hk. 2022-2025. All rights reserved.
# File name: Makefile
# Author   : 苏木
# Date     : 2024-11-09
# Version  : 
# Description: 
##============================================================================#
##

include ${COMPILE_TOOL_CFG} # 工具配置
include ${INCLUDE_CFG} # 头文件目录配置
include ${SOURCE_CFG}  # 源文件目录配置

ifdef IMX_NAME
TARGET          ?= $(IMX_NAME)
else  
TARGET          ?= default
endif  

LIBFLAG			+= -lgcc 
LIBDIRS         += -L$(ARM_GCC_LIB)#/home/sumu/2software/gcc-arm-linux-gnueabihf-8.3.0/lib/gcc/arm-linux-gnueabihf/8.3.0 # 编译器库所在的目录

OBJDIRS         := obj

ifdef LINKER_FILE
LD_FILE         := $(LINKER_FILE)
else
LD_FILE         := imx6ul.lds
#LD_FILE         := section_test.lds
endif
INCLUDE			:= $(patsubst %, -I %, $(INCDIRS))

SFILES			:= $(foreach dir, $(SRCDIRS), $(wildcard $(dir)/*.S))
CFILES			:= $(foreach dir, $(SRCDIRS), $(wildcard $(dir)/*.c))

SFILENDIR		:= $(notdir  $(SFILES))
CFILENDIR		:= $(notdir  $(CFILES))

SOBJS			:= $(patsubst %, $(OBJDIRS)/%, $(SFILENDIR:.S=.o))
COBJS			:= $(patsubst %, $(OBJDIRS)/%, $(CFILENDIR:.c=.o))
OBJS			:= $(SOBJS) $(COBJS)

VPATH			:= $(SRCDIRS)

# makefile获取工程的git版本号及远程分支
#GIT_SHA = $(shell git rev-list HEAD | awk 'NR==1')
GIT_SHA = $(shell git rev-parse --short HEAD | awk 'NR==1')
GIT_SEQ = $(shell git rev-list HEAD | wc -l)
GIT_VER_INFO = $(GIT_SHA)-$(GIT_SEQ)
GIT_SVR_PATH = $(shell git remote -v | awk 'NR==1' | sed 's/[()]//g' | sed 's/\t/ /g' |cut -d " " -f2)

ifneq ($(GIT_VER_INFO),)
	CFLAGS += -DGIT_VERSION=\"$(GIT_VER_INFO)\"
else
	CFLAGS += -DGIT_VERSION=\"unknown\"
endif

ifneq ($(GIT_SVR_PATH),)
	CFLAGS += -DGIT_PATH=\"$(GIT_SVR_PATH)\"
else
	CFLAGS += -DGIT_PATH=\"unknown\"
endif

$(TARGET).bin : $(OBJS)
	$(LD) -T$(LD_FILE) -o $(TARGET).elf $^ $(LIBFLAG) $(LIBDIRS)
	$(OBJCOPY) -O binary -S $(TARGET).elf $@
	$(OBJDUMP) -D -m arm $(TARGET).elf > $(TARGET).dis

$(SOBJS) : $(OBJDIRS)/%.o : %.S
	$(CC) $(SFLAGS) -c $(INCLUDE) -o $@ $<

$(COBJS) : $(OBJDIRS)/%.o : %.c
	$(CC) $(CFLAGS) -c $(INCLUDE) -o $@ $<

.PHONY: clean
clean:
	rm -rf $(TARGET).elf $(TARGET).dis $(TARGET).bin $(TARGET).imx $(TARGET)_mkimage.imx $(COBJS) $(SOBJS)

	
print:
	@echo "IMX_NAME:$(IMX_NAME)"
	@echo "TARGET  :$(TARGET)"
	@echo "OBJDIRS :$(OBJDIRS)"
	