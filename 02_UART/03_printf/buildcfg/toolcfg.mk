# Makefile文件工具配置
CROSS_COMPILE   ?= arm-linux-gnueabihf-
CC 		        := $(CROSS_COMPILE)gcc
LD		        := $(CROSS_COMPILE)ld
OBJCOPY         := $(CROSS_COMPILE)objcopy
OBJDUMP         := $(CROSS_COMPILE)objdump

CFLAGS          += -g 
CFLAGS          += -Wall      # 输出所有警告选项（ -Wall）
CFLAGS          += -O2        # 代码优化选项,产生尽可能小和快的代码。如无特殊要求，不建议使用 O2 以上的优化。
CFLAGS          += -nostdlib  # 使用 -nostdlib 选项可以告诉编译器不要链接标准库，这样生成的可执行文件就不依赖于标准库了。
                              # 在这种情况下，如果程序需要标准库的功能，你需要自己提供相应的实现或者使用其他替代方案。
CFLAGS          += -Werror    # 将全部的 warning 当成error
CFLAGS          += -fno-builtin # 选项“-fno-builtin”表示不使用内建函数，这样我们就可以自己实现 putc和 puts 这样的函数了。
SFLAGS          += -g -Wall -nostdlib -fno-builtin -O2 -Werror
