#include "cc.h"
#include "bsp_clk.h"
#include "bsp_delay.h"
#include "bsp_led.h"
#include "bsp_uart.h"
#include "stdio.h"
int main(int argc, const char * argv[])
{
	unsigned char state = OFF;
	int a , b;

	clk_enable(); /* 使能所有的时钟 			*/
	led_init();	  /* 初始化led 			*/

	uart_init(); /* 初始化串口，波特率115200 */

	while (1)
	{
		printf("输入两个整数，使用空格隔开:");
		scanf("%d %d", &a, &b);					 	     	/* 输入两个整数 */
		printf("\r\n数据%d + %d = %d\r\n\r\n", a, b, a+b);	/* 输出两个数相加的和 */

		state = !state;
		led_switch(LED0, state);
	}

	return 0;
}
