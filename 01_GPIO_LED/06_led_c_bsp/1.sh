#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : 1.sh
# * Author     : 苏木
# * Date       : 2024-11-08
# * ======================================================
##

##======================================================
BLACK="\033[1;30m"
RED='\033[1;31m'    # 红
GREEN='\033[1;32m'  # 绿
YELLOW='\033[1;33m' # 黄
BLUE='\033[1;34m'   # 蓝
PINK='\033[1;35m'   # 紫
CYAN='\033[1;36m'   # 青
WHITE='\033[1;37m'  # 白
CLS='\033[0m'       # 清除颜色

INFO="${GREEN}[INFO]${CLS}"
WARN="${YELLOW}[WARN]${CLS}"
ERR="${RED}[ERR ]${CLS}"

SCRIPT_NAME=${0#*/}
SCRIPT_CURRENT_PATH=${0%/*}
SCRIPT_ABSOLUTE_PATH=`cd $(dirname ${0}); pwd`

SYSTEM_ENVIRONMENT_FILE=/etc/profile # 系统环境变量位置
USER_ENVIRONMENT_FILE=~/.bashrc
SOFTWARE_DIR_PATH=~/2software        # 软件安装目录

TIME_START=
TIME_END=

#===============================================
function get_start_time()
{
	TIME_START=$(date +'%Y-%m-%d %H:%M:%S')
}
function get_end_time()
{
	TIME_END=$(date +'%Y-%m-%d %H:%M:%S')
}

function get_execute_time()
{
	start_seconds=$(date --date="$TIME_START" +%s);
	end_seconds=$(date --date="$TIME_END" +%s);
	duration=`echo $(($(date +%s -d "${TIME_END}") - $(date +%s -d "${TIME_START}"))) | awk '{t=split("60 s 60 m 24 h 999 d",a);for(n=1;n<t;n+=2){if($1==0)break;s=$1%a[n]a[n+1]s;$1=int($1/a[n])}print s}'`
	echo "===*** 运行时间：$((end_seconds-start_seconds))s,time diff: ${duration} ***==="
}

function get_ubuntu_info()
{
    # 获取内核版本信息
    local kernel_version=$(uname -r) # -a选项会获得更详细的版本信息
    # 获取Ubuntu版本信息
    local ubuntu_version=$(lsb_release -ds)

    # 获取Ubuntu RAM大小
    local ubuntu_ram_total=$(cat /proc/meminfo |grep 'MemTotal' |awk -F : '{print $2}' |sed 's/^[ \t]*//g')
    # 获取Ubuntu 交换空间swap大小
    local ubuntu_swap_total=$(cat /proc/meminfo |grep 'SwapTotal' |awk -F : '{print $2}' |sed 's/^[ \t]*//g')
    #显示硬盘，以及大小
    #local ubuntu_disk=$(sudo fdisk -l |grep 'Disk' |awk -F , '{print $1}' | sed 's/Disk identifier.*//g' | sed '/^$/d')
    
    #cpu型号
    local ubuntu_cpu=$(grep 'model name' /proc/cpuinfo |uniq |awk -F : '{print $2}' |sed 's/^[ \t]*//g' |sed 's/ \+/ /g')
    #物理cpu个数
    local ubuntu_physical_id=$(grep 'physical id' /proc/cpuinfo |sort |uniq |wc -l)
    #物理cpu内核数
    local ubuntu_cpu_cores=$(grep 'cpu cores' /proc/cpuinfo |uniq |awk -F : '{print $2}' |sed 's/^[ \t]*//g')
    #逻辑cpu个数(线程数)
    local ubuntu_processor=$(grep 'processor' /proc/cpuinfo |sort |uniq |wc -l)
    #查看CPU当前运行模式是64位还是32位
    local ubuntu_cpu_mode=$(getconf LONG_BIT)

    # 打印结果
    echo "ubuntu: $ubuntu_version - $ubuntu_cpu_mode"
    echo "kernel: $kernel_version"
    echo "ram   : $ubuntu_ram_total"
    echo "swap  : $ubuntu_swap_total"
    echo "cpu   : $ubuntu_cpu,physical id is$ubuntu_physical_id,cores is $ubuntu_cpu_cores,processor is $ubuntu_processor"
}
#===============================================
# 开发环境信息
function dev_env_info()
{
    echo "Development environment: "
    echo "ubuntu : 20.04.2-64(1核12线程 16GB RAM,512GB SSD)"
    echo "VMware : VMware® Workstation 17 Pro 17.6.0 build-24238078"
    echo "Windows: "
    echo "          处理器 AMD Ryzen 7 5800H with Radeon Graphics 3.20 GHz 8核16线程"
    echo "          RAM	32.0 GB (31.9 GB 可用)"
    echo "          系统类型	64 位操作系统, 基于 x64 的处理器"
    echo "说明: 初次安装完SDK,在以上环境下编译大约需要3小时,不加任何修改进行编译大约需要10~15分钟左右"
}
#===============================================
TARGET_NAME=bare_demo
TARGET_BIN_FILE=${TARGET_NAME}.bin
TARGET_IMX_FILE=${TARGET_NAME}.imx
MAKE_PARAM[${#MAKE_PARAM[*]}]="IMX_NAME=${TARGET_NAME}"

IMXDOWNLOAD_TOOL=../../tool/imxdownload/imxdownload
MKIMAGE_TOOL=../../tool/mkimage/mkimage
IMXIMAGE_CFG=../../tool/mkimage/u-boot-dtb.cfgout

IMGUTIL_TOOL_PATH=../../tool/imgutil/evkmcimx6ull
IMGUTIL_BIN_NAME=sdk20-app.bin
IMGUTIL_IMG_NAME=sdk20-app.img
IMGUTIL_TOOL=../../tool/imgutil/evkmcimx6ull/mkimage.sh

SD_NODE=/dev/sdc

ARCH_NAME=arm
MAKE_PARAM[${#MAKE_PARAM[*]}]="ARCH=${ARCH_NAME}"
CROSS_COMPILE_PREFIX=arm-linux-gnueabihf- # arm-none-eabi-
MAKE_PARAM[${#MAKE_PARAM[*]}]="CROSS_COMPILE=${CROSS_COMPILE_PREFIX}"
ARM_GCC=${CROSS_COMPILE_PREFIX}gcc
ARM_GCC_PATH=$(which ${ARM_GCC})  # /home/sumu/2software/gcc-arm-linux-gnueabihf-8.3.0/bin/arm-linux-gnueabihf-gcc
ARM_GCC_MAJOR=$(echo __GNUC__ | $ARM_GCC -E -xc - | tail -n 1)
ARM_GCC_MINOR=$(echo __GNUC_MINOR__ | $ARM_GCC -E -xc - | tail -n 1)
ARM_GCC_PATCHLEVEL=$(echo __GNUC_PATCHLEVEL__ | $ARM_GCC -E -xc - | tail -n 1)
ARM_GCC_VERSION=$(${ARM_GCC} -dumpversion)
ARM_GCC_LIB_PATH_TEMP[0]=${ARM_GCC_PATH%/*}     # 去掉arm-linux-gnueabihf-gcc这一级目录 /home/sumu/2software/gcc-arm-linux-gnueabihf-8.3.0/bin
ARM_GCC_LIB_PATH_TEMP[1]=${ARM_GCC_LIB_PATH_TEMP%/*} # 去掉bin这一级目录，得到安装目录 /home/sumu/2software/gcc-arm-linux-gnueabihf-8.3.0
# /home/sumu/2software/gcc-arm-linux-gnueabihf-8.3.0/lib/gcc/arm-linux-gnueabihf/8.3.0
ARM_GCC_LIB_PATH=${ARM_GCC_LIB_PATH_TEMP[1]}/lib/gcc/${CROSS_COMPILE_PREFIX%-*}/${ARM_GCC_VERSION}
if [ -d "${ARM_GCC_LIB_PATH}" ];then
    MAKE_PARAM[${#MAKE_PARAM[*]}]="ARM_GCC_LIB=${ARM_GCC_LIB_PATH}"
fi

CROSS_COMPILE_LD_FILE=imx6ul.lds
CROSS_COMPILE_LD_ADDR=0x87800000

# Makefile子文件路径
MAKEFILE_COMPILE_TOOL_CFG=${SCRIPT_ABSOLUTE_PATH}/buildcfg/toolcfg.mk
MAKEFILE_INCLUDE_CFG=${SCRIPT_ABSOLUTE_PATH}/buildcfg/includecfg.mk
MAKEFILE_SOURCE_CFG=${SCRIPT_ABSOLUTE_PATH}/buildcfg/sourcecfg.mk

MAKE_PARAM[${#MAKE_PARAM[*]}]="COMPILE_TOOL_CFG=${MAKEFILE_COMPILE_TOOL_CFG}"
MAKE_PARAM[${#MAKE_PARAM[*]}]="INCLUDE_CFG=${MAKEFILE_INCLUDE_CFG}"
MAKE_PARAM[${#MAKE_PARAM[*]}]="SOURCE_CFG=${MAKEFILE_SOURCE_CFG}"

MAKE_PARAM[${#MAKE_PARAM[*]}]="LINKER_FILE=${CROSS_COMPILE_LD_FILE}"

function time_count_down
{
    for i in {3..0}
        do     
            echo -ne "${INFO}after ${i} is end!!"
            echo -ne "\r\r"        # echo -e 处理特殊字符  \r 光标移至行首，但不换行
            sleep 1
        done
    echo "" # 打印一个空行，防止出现混乱
}

function clean_project()
{
    make ${MAKE_PARAM[@]} clean
}

function build_project()
{
    get_start_time
    make V=0 ${MAKE_PARAM[@]} # 生成的是bin文件
    
    echo -e "${INFO}检查是否编译成功..."
    if [ ! -f "${TARGET_BIN_FILE}" ];then
        echo -e "${ERR}${TARGET_BIN_FILE} 编译失败,请检查后重试"
    else
        echo -e "${INFO}${TARGET_BIN_FILE} 编译成功"
    fi
    get_end_time
    get_execute_time
}

# mkimage需要先创建imx文件，然后调用dd进行下载
function mkimage_download2sd()
{
    
    echo -e "${WARN}查看sd相关节点, 将使用${SD_NODE},3秒后继续..."
    ls /dev/sd*
    time_count_down
    # 判断SD卡节点是否存在
    if [ ! -e "${SD_NODE}" ];then
        echo -e "${ERR}${SD_NODE}不存在,请检查SD卡是否插入..."
        return
    fi

    # 检查bin文件是否存在
    if [ ! -f "${TARGET_BIN_FILE}" ];then
        echo -e "${ERR}${TARGET_BIN_FILE} 不存在,请检查后再下载..."
        return
    fi

    # 重新生成一个imx文件
    echo -e "${INFO}生成imx文件 ${TARGET_IMX_FILE}"
    # ./mkimage -n ./imximage.cfg -T imximage -e 0X87800000 -d led.bin led.imx
    echo -e "${INFO}${MKIMAGE_TOOL} -n ${IMXIMAGE_CFG} -T imximage -e ${CROSS_COMPILE_LD_ADDR} -d ${TARGET_BIN_FILE} ${TARGET_IMX_FILE}"
    ${MKIMAGE_TOOL} -n ${IMXIMAGE_CFG} -T imximage -e ${CROSS_COMPILE_LD_ADDR} -d ${TARGET_BIN_FILE} ${TARGET_IMX_FILE}

    echo -e "${INFO}3s后开始下载 ${TARGET_IMX_FILE} 到 ${SD_NODE}..."
    time_count_down
    # sudo dd if=led.imx of=/dev/sdc bs=1k seek=1 conv=fsync
    sudo dd if=${TARGET_IMX_FILE} of=${SD_NODE} bs=1k seek=1 conv=fsync
}

# imxdownload工具使用bin文件，会创建imx文件然后下载到sd卡
function imxdownload_download2sd()
{

    echo -e "${WARN}查看sd相关节点, 将使用${SD_NODE},3秒后继续..."
    ls /dev/sd*
    time_count_down
    if [ ! -e "${SD_NODE}" ];then
        echo -e "${ERR}${SD_NODE}不存在,请检查SD卡是否插入..."
        return
    fi

    # 检查bin文件是否存在
    if [ ! -f "${TARGET_BIN_FILE}" ];then
        echo -e "${ERR}${TARGET_BIN_FILE} 不存在,请检查后再下载..."
        return
    fi
    echo -e "${INFO}3s后开始下载 ${TARGET_IMX_FILE} 到 ${SD_NODE}..."
    time_count_down
    ${IMXDOWNLOAD_TOOL} ${TARGET_BIN_FILE} ${SD_NODE}

}

# imgutil工具使用bin文件，会创建img文件然后下载到sd卡
function imgutil_download2sd()
{
    echo -e "${WARN}查看sd相关节点, 将使用${SD_NODE},3秒后继续..."
    ls /dev/sd*
    time_count_down
    if [ ! -e "${SD_NODE}" ];then
        echo -e "${ERR}${SD_NODE}不存在,请检查SD卡是否插入..."
        return
    fi

    # 检查bin文件是否存在
    if [ ! -f "${TARGET_BIN_FILE}" ];then
        echo -e "${ERR}${TARGET_BIN_FILE} 不存在,请检查后再下载..."
        return
    fi
    # 拷贝到imgutil工作目录并重命名
    cp -avf ${TARGET_BIN_FILE} ${IMGUTIL_TOOL_PATH}/${IMGUTIL_BIN_NAME}
    # 开始生成可以下载到sd卡的img文件
    cd ${IMGUTIL_TOOL_PATH}
    ./mkimage.sh sd ${CROSS_COMPILE_LD_ADDR}
    echo ""
    if [ ! -e "${IMGUTIL_IMG_NAME}" ];then
        echo -e "${ERR}${IMGUTIL_IMG_NAME}不存在,请检查后再下载..."
        return
    fi
    echo -e "${INFO}3s后开始下载 ${TARGET_IMX_FILE} 到 ${SD_NODE}..."
    time_count_down
    # sudo dd if=led.img of=/dev/sdc bs=512 conv=fsync
    sudo dd if=${IMGUTIL_IMG_NAME} of=${SD_NODE} bs=512 conv=fsync
}
# 清空SD卡
function clear_sd()
{
    echo -e "${WARN}查看sd相关节点, 将使用${SD_NODE},3秒后继续..."
    ls /dev/sd*
    time_count_down
    if [ ! -e "${SD_NODE}" ];then
        echo -e "${ERR}${SD_NODE}不存在,请检查SD卡是否插入..."
        return
    fi
    echo -e "${INFO}${SD_NODE}存在,正在清空前5M空间..."
    sudo dd if=/dev/zero of=/dev/sdc bs=512 count=10240 conv=fsync
}
function build_download_project()
{
    build_project
    # imxdownload_download2sd
    mkimage_download2sd # 和uboot使用的工具一致，后面都用这个了
    # imgutil_download2sd
}

# 打印传输给make的各个参数
function func_print_make_param()
{
    for temp in ${MAKE_PARAM[@]}
        do
            echo -e "\t${temp}"
        done
}

function echo_menu()
{
    echo "================================================="
	echo -e "${GREEN}               build project ${CLS}"
	echo -e "${GREEN}                by @苏木    ${CLS}"
	echo "================================================="
    echo -e "${PINK}current path         :$(pwd)${CLS}"
    echo -e "${PINK}SCRIPT_CURRENT_PATH  :${SCRIPT_CURRENT_PATH}${CLS}"
    echo -e "${PINK}ARM_GCC_VERSION      :${ARM_GCC_VERSION}${CLS}"
    echo ""
    echo -e "*${GREEN} [0] 只编译工程源码${CLS}"
    echo -e "* [1] 编译源码并下载到SD卡"
    echo -e "* [2] 清理工程文件"
    echo -e "* [3] 清空SD卡(清空前面5M空间)"
    echo "================================================="
    echo "make param:"
    func_print_make_param
    echo "================================================="
}

function func_process()
{
	read -p "请选择功能,默认选择0:" choose
	case "${choose}" in
		"0") build_project;;
		"1") build_download_project;;
		"2") clean_project;;
		"3") clear_sd;;
		*) build_project;;
	esac
}

echo_menu
func_process
