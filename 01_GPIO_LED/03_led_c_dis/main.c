#include "led.h"

void delay(volatile unsigned int n)
{
	while(n--);
}

int  main()
{
	led_init();

	while(1)
	{
		led_ctl(1);
		delay(10000000);
		led_ctl(0);
		delay(10000000);
	}
					
	return 0;
}