#include "led.h"

static volatile unsigned int *CCM_CCGR1;
static volatile unsigned int *SW_MUX_CTL_PAD_GPIO1_IO03;
static volatile unsigned int *GPIO1_GDIR;
static volatile unsigned int *GPIO1_DR;

void led_init(void)
{
	unsigned int val;
	
	CCM_CCGR1                 = (volatile unsigned int *)(0x020C406C);
	SW_MUX_CTL_PAD_GPIO1_IO03 = (volatile unsigned int *)(0X020E0068);
	GPIO1_DR                  = (volatile unsigned int *)(0X0209C000);
	GPIO1_GDIR                = (volatile unsigned int *)(0X0209C000 + 0x4);
	/* GPIO1_IO03 */
	/* a. 使能GPIO1,其实不设置默认也是使能的
	 *    set CCM to enable GPIO1
	 *    gpio1 clock (gpio1_clk_enable): CCM_CCGR1[CG13] 0X020C406C
	 *    bit[27:26] = 0b11
	 */
	*CCM_CCGR1 |= (3<<26);
	
	/* b. 设置GPIO1_IO03用于GPIO
	 *    set SW_MUX_CTL_PAD_GPIO1_IO03 to configure GPIO1_IO03 as GPIO
	 *    SW_MUX_CTL_PAD_GPIO1_IO03  0X020E0068
	 *    bit[3:0] = 0b0101 alt5
	 */
	val = *SW_MUX_CTL_PAD_GPIO1_IO03;
	val &= ~(0xf);
	val |= (0x5);
	*SW_MUX_CTL_PAD_GPIO1_IO03 = val;
	
	
	/* c. 设置GPIO1_IO03作为output引脚
	 *    set GPIO1_GDIR to configure GPIO1_IO03 as output
	 *    GPIO1_GDIR  0X0209C000 + 0x4
	 *    bit[3] = 0b1
	 */
	*GPIO1_GDIR |= (1<<3);

}

void led_ctl(int on)
{
	if (on) /* on: output 0*/
	{
		/* d. 设置GPIO1_DR输出低电平
		 *    set GPIO1_DR to configure GPIO1_IO03 output 0
		 *    GPIO1_DR 0X0209C000 + 0
		 *    bit[3] = 0b0
		 */
		*GPIO1_DR &= ~(1<<3);
	}
	else  /* off: output 1*/
	{
		/* e. 设置GPIO1_IO3输出高电平
		 *    set GPIO1_DR to configure GPIO1_IO03 output 1
		 *    GPIO1_DR 0X0209C000 + 0
		 *    bit[3] = 0b1
		 */ 
		*GPIO1_DR |= (1<<3);
	}
}
