#include "stdio.h"
#include "cc.h"
#include "bsp_clk.h"
#include "bsp_delay.h"
#include "bsp_led.h"
#include "bsp_uart.h"
#include "bsp_beep.h"
#include "bsp_key.h"
#include "bsp_int.h"
#include "bsp_exit.h"

void printException(unsigned int cpsr, char *str)
{
	printf("Exception! cpsr is 0x%x\r\n", cpsr);
	printf("%s\r\n", str);
}

void printSWIVal(unsigned int *pSWI)
{
	printf("SWI val = 0x%x\r\n", *pSWI & ~0xff000000);
}

void system_init(void)
{
	int_init(); 		/* 初始化中断(一定要最先调用，在这里初始化中断向量表) */
	imx6u_clkinit();	/* 初始化系统时钟 */
	clk_enable();       /* 使能所有的时钟 */
	uart_init();        /* 初始化串口，波特率115200 */
	printf("system_init finished!!!\r\n");
}

int main(int argc, const char * argv[])
{
	unsigned char state = OFF;

	led_init();	  /* 初始化led  */
	beep_init();  /* 初始化beep */
	key_init();	  /* 初始化key  */
	exit_init();  /* 初始化按键中断 */

	while(1)			
	{	
		state = !state;
		led_switch(LED0, state);
		delay(500);
	}

	return 0;
}

