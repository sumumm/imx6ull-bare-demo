# Makefile文件工具配置
CROSS_COMPILE   ?= arm-linux-gnueabihf-
CC 		        := $(CROSS_COMPILE)gcc
LD		        := $(CROSS_COMPILE)ld
OBJCOPY         := $(CROSS_COMPILE)objcopy
OBJDUMP         := $(CROSS_COMPILE)objdump

CFLAGS          += -Wall -Wa,-mimplicit-it=thumb -nostdlib -fno-builtin -O2 -Werror
SFLAGS          += -Wall -Wa,-mimplicit-it=thumb -nostdlib -fno-builtin -O2 -Werror
