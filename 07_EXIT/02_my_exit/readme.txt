/**
 *********************************************************
 * 实验简介
 * 实验名称：外部中断实验——自己实现GIC相关操作(参考韦东山imx6ull裸机开发教程)
 * 实验平台：正点原子 I.MX6U-ALPHA 开发板
 * 实验目的：xxx

 *********************************************************
 * 硬件资源及引脚分配
 * 1 LED灯
     LED0(RED)    :  GPIO_3
 * 2 串口
     UART1

 *********************************************************
 * 实验现象
 * 1 可以打印对应的信息。

 *********************************************************
 * 注意事项
 * 1 注意拨码开关拨到从SD卡启动
 * 2 串口线接到USB_TTL
 * 3 这里我没有调试，直接照搬教程源码，主要是为了加深理解而已。
 *********************************************************
 */
 