#include "uart.h"
#include "my_printf.h"

char g_charA = 'A';			//存储在 .data段
const char g_charB = 'B';	//存储在 .rodata段
const char g_charC;			//存储在 .bss段
int g_intA = 0;				//存储在 .bss段
int g_intB;					//存储在 .bss段

void delay (volatile int time)
{
	while(time--);
}

int main(int argc, const char * argv[])
{
	Uart_Init();	//初始化uart串口

	printf("03_without_relocation test!\n\r");
	printf("g_charA=%d &g_charA=0x%x\n\r", g_charA, &g_charA);
	printf("g_charB=%d &g_charB=0x%x\n\r", g_charB, &g_charB);
	printf("g_charC=%d &g_intA=0x%x\n\r", g_charC, &g_charC);
	printf("g_intA=%d &g_intA=0x%x\n\r", g_intA, &g_intA);
	printf("g_intB=%d &g_intB=0x%x\n\r", g_intB, &g_intB);

	/* 在串口上输出g_charA */
	while (1)
	{
		//put_char(g_charA);
		g_charA++;
		delay(1000000);
	}

	return 0;
}


