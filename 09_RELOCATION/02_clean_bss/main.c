#include "my_printf.h"
#include "uart.h"

char g_charA = 'A';			//存储在 .data段
const char g_charB = 'B';	//存储在 .rodata段
const char g_charC;			//存储在 .bss段
int g_intA = 0;				//存储在 .bss段
int g_intB;					//存储在 .bss段

void delay (volatile int time)
{
	while(time--);
}

int main(int argc, const char * argv[])
{
	Uart_Init();	//初始化uart串口

	printf("g_intA = 0x%08x &g_intA = 0x%x\n\r", g_intA, &g_intA);	//打印g_intA的值
	printf("g_intB = 0x%08x &g_intB = 0x%x\n\r", g_intB, &g_intB);	//打印g_intB的值
	return 0;
}


