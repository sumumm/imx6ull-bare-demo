
void copy_data (void)
{
	/* 从链接脚本中获得参数 data_load_addr, data_start, data_end */
	extern int data_load_addr, data_start, data_end;

	volatile unsigned int *dest = (volatile unsigned int *)&data_start;
	volatile unsigned int *end = (volatile unsigned int *)&data_end;
	volatile unsigned int *src = (volatile unsigned int *)&data_load_addr;

	/* 重定位数据 */
	while (dest < end)
	{
		*dest++ = *src++;
	}
}

void clean_bss(void)
{
	/* 从lds文件中获得 __bss_start, __bss_end */
	extern int __bss_end, __bss_start;

	volatile unsigned int *start = (volatile unsigned int *)&__bss_start;
	volatile unsigned int *end = (volatile unsigned int *)&__bss_end;

	while (start <= end)
	{
		*start++ = 0;
	}
}
